__version__ = (0, 5, 9)

version_string = '.'.join(str(n) for n in __version__)
