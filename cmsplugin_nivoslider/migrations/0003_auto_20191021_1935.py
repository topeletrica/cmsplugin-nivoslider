# -*- coding: utf-8 -*-
# Generated by Django 1.11.25 on 2019-10-21 22:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cmsplugin_nivoslider', '0002_auto_20191017_2147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sliderplugin',
            name='theme',
            field=models.CharField(choices=[('bar', 'bar'), ('dark', 'dark'), ('default', 'default'), ('topeletrica', 'topeletrica'), ('light', 'light')], default='default', max_length=50, verbose_name='theme'),
        ),
    ]
