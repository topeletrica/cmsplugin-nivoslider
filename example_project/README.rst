An example project to give this plugin a try.

Install dependencies using ``pip install -r requirements.txt`` (may need sudo).

Run ``python manage.py runserver``, then go to localhost:8000 in your web
browser.  Everything will be explained there.
